from django.db import models
from django.db.models.deletion import CASCADE

from .category import Category


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Name', max_length=30)
    price = models.PositiveIntegerField(default=0)
    photo = models.CharField('url image', max_length=30)
    characteristics = models.TextField('characteristics')
    category = models.ForeignKey(
        Category, on_delete=CASCADE, related_name="product")
