from django.db.models import fields
from productApp.models.category import Category
from rest_framework import serializers
from .productSerializer import ProductSerializer
from productApp.models.product import Product

from productApp.serializers import productSerializer


class CategorySerializer(serializers.ModelSerializer):
    # product = ProductSerializer()

    class Meta:
        model = Category
        fields = ['name']

    # def create(self, validated_data):
    #     ProductData = validated_data.pop('product')
    #     categoryInstance = Category.objects.create(**validated_data)
    #     Product.objects.create(category=categoryInstance, **ProductData)
    #     return categoryInstance

    # def to_representation(self, obj):
    #     category = Category.objects.get(id=obj.id)
    #     product = Product.objects.get(category=obj.id)
    #     return{
    #         'id': category.id,
    #         'name': category.name,
    #         'product': {
    #             'id': product.id,
    #             'name': product.name,
    #             'price': product.price,
    #             'photo': product.photo,
    #             'characteristics': product.characteristics
    #         }
    #     }
